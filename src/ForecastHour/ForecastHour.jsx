/** @format */

import React from 'react';
import { Typography } from '@material-ui/core';

import styles from './ForecastHour.module.css';

// import PropTypes from 'prop-types';
// import SmallLabel from './SmallLabel';
// import Text from './Text';
// import device from '../responsive/Device';

const ForecastHour = (props) => {
    const { temp, month, day, hour, icon } = props;
    const iconUrl = `https://openweathermap.org/img/w/${icon}.png`;
    const time = hour.split(':');

    return (
        <div className={styles.forecastWrapper}>
            <Typography align='center' color='textPrimary'>
                {month} {day}
            </Typography>
            <Typography align='center' color='textPrimary'>{`${time[0]}:${time[1]} ${
                time[2].split(' ')[1]
            }`}</Typography>
            <img className={styles.weatherIcon} alt={iconUrl} src={iconUrl} />
            <Typography align='center' weight='400' color='textPrimary'>
                {temp}
                <sup>&#176;C</sup>
            </Typography>
        </div>
    );
};

// ForecastHour.propTypes = {
//   temp: PropTypes.number.isRequired,
//   month: PropTypes.string.isRequired,
//   day: PropTypes.string.isRequired,
//   hour: PropTypes.number.isRequired,
//   icon: PropTypes.string.isRequired,
// };

export default ForecastHour;
