/** @format */

import React from 'react';
import styles from './WeatherInfo.module.css';
import { Typography } from '@material-ui/core';

const WeatherInfo = ({
    weather: {
        city,
        icon,
        country,
        description,
        lowestTemp,
        highestTemp,
        wind,
        sunrise,
        sunset,
        humidity,
        temp,
    },
}) => {
    return (
        <div className={styles.currentWeather}>
            <h2 className={styles.currentWeather__city}>
                <span>{city}</span>
                <sup>{country}</sup>
            </h2>
            <div className='city-temp'>
                <h1>
                    <span className={styles.currentWeather__temp}>{Math.round(temp)}</span>
                    <sup>&deg;C</sup>
                </h1>
            </div>
            <div className={styles.info}>
                <img
                    className='city-icon'
                    width='74'
                    height='74'
                    src={`https://openweathermap.org/img/wn/${icon}@2x.png`}
                    alt={description}
                />
                <Typography color='textSecondary'>{description}</Typography>
            </div>
            <div className={styles.currentWeather__moreInfo}>
                <div>
                    <Typography color='textPrimary'>High</Typography>
                    <Typography color='textSecondary'>
                        {highestTemp}
                        <sup>&deg;C</sup>
                    </Typography>
                </div>
                <div>
                    <Typography color='textPrimary'>Low</Typography>
                    <Typography color='textSecondary'>
                        {lowestTemp}
                        <sup>&deg;C</sup>
                    </Typography>
                </div>
                <div>
                    <Typography color='textPrimary'>Humidity</Typography>
                    <Typography color='textSecondary'>{humidity}%</Typography>
                </div>
                <div>
                    <Typography color='textPrimary'>Wind</Typography>
                    <Typography color='textSecondary'>{wind} m/sec</Typography>
                </div>
                <div>
                    <Typography color='textPrimary'>Sunrise</Typography>
                    <Typography color='textSecondary'>{sunrise} AM</Typography>
                </div>
                <div>
                    <Typography color='textPrimary'>Sunset</Typography>
                    <Typography color='textSecondary'>{sunset} PM</Typography>
                </div>
            </div>
        </div>
    );
};
export default WeatherInfo;
