/** @format */

import React, { useState } from 'react';
import { InputAdornment, TextField } from '@material-ui/core';
import { SearchOutlined } from '@material-ui/icons';
import styles from './SearchBar.module.css';

const SearchBar = ({ search }) => {
    const [query, setQuery] = useState('');
    return (
        <div className={styles.formControl}>
            <TextField
                id='outlined-basic'
                className={styles.search}
                // placeholder='Enter City Name'
                label='Enter City Name'
                value={query}
                variant='outlined'
                onChange={(e) => setQuery(e.target.value)}
                onKeyPress={search}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position='start'>
                            <SearchOutlined />
                        </InputAdornment>
                    ),
                }}
            />
        </div>
    );
};
export default SearchBar;
