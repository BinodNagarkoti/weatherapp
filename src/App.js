/** @format */

import React, { useState } from 'react';
import styles from './App.module.css';
import { Typography } from '@material-ui/core';
import { fetchWeather, fetchForecastWeather } from './api';
import ForecastHour from './ForecastHour/ForecastHour';
import SearchBar from './SearchBar/SearchBar';
import WeatherInfo from './WeatherInfo/WeatherInfo';
// import Footer from './Footer/Footer';
function App() {
    const [query, setQuery] = useState('');
    const [weather, setWeather] = useState({});
    const [forecast, setForecast] = useState([]);

    const [Loading, setLoading] = useState(false);
    const [isResult, setIsResult] = useState(false);
    const search = async (e) => {
        if (e.key === 'Enter') {
            const value = e.target.value;
            console.log(`Entered : ${e.target.value}`);
            setLoading(true);
            const fetchedData = await fetchWeather(value);
            setWeather(fetchedData);
            const fetchedForcastWeather = await fetchForecastWeather(value);
            setForecast(fetchedForcastWeather);
            setQuery('');
            setLoading(false);
            setIsResult(true);

            console.log(forecast);
        }
    };
    console.log(`month,day,hour:${forecast[1]}`);
    const forecast__ = forecast.map((item) => (
        <ForecastHour
            key={item.dt}
            temp={Math.floor(item.main.temp * 1) / 1}
            icon={item.weather[0].icon}
            month={new Date(item.dt_txt).toDateString().split(' ')[1]}
            day={`${new Date(item.dt_txt).toDateString().split(' ')[0]} ${
                new Date(item.dt_txt).toDateString().split(' ')[2]
            } `}
            hour={new Date(item.dt_txt).toLocaleTimeString()}
        />
    ));
    return (
        <main>
            <Typography
                variant='h4'
                component='h4'
                className={styles.appName}
                color='textSecondary'
            >
                Weather App
            </Typography>
            <SearchBar querys={query} search={search} />
            {Loading ? <div className={styles.result}> Loading </div> : ''}
            {/* </div> */}
            <div className={styles.result}>
                {Loading
                    ? ''
                    : weather.main && (
                          <div>
                              <WeatherInfo weather={weather} />
                              <div className={styles.forecastWrapper}>
                                  <div className={styles.forecast}>{forecast__}</div>
                              </div>
                          </div>
                      )}
            </div>
        </main>
    );
}

export default App;
