/** @format */

import axios from 'axios';

const URL = 'https://api.openweathermap.org/data/2.5/weather';
const URL2 = 'https://api.openweathermap.org/data/2.5/forecast';
// https://api.openweathermap.org/data/2.5/weather?q=peru&units=metric&appid=287458ecb6a715b129a964958ad91b65
const API_KEY = '287458ecb6a715b129a964958ad91b65';
export const fetchWeather = async (query) => {

    const { data } = await axios.get(URL, {
        params: {
            q: query,
            units: 'metric',
            APPID: API_KEY,
        },
    });
    // console.log(data);
    const sunset = new Date(data.sys.sunset * 1000).toLocaleTimeString().slice(0, 4);
    const sunrise = new Date(data.sys.sunrise * 1000).toLocaleTimeString().slice(0, 4);
    const modifiedData = {
        city: data.name,
        country: data.sys.country,
        // date,
        description: data.weather[0].description,
        main: data.weather[0].main,
        temp: data.main.temp,
        highestTemp: data.main.temp_max,
        lowestTemp: data.main.temp_min,
        sunrise,
        sunset,
        icon: data.weather[0].icon,
        clouds: data.clouds.all,
        humidity: data.main.humidity,
        wind: data.wind.speed,
    };

    return modifiedData;
};
export const fetchForecastWeather = async (query) => {

    const { data } = await axios.get(URL2, {
        params: {
            q: query,
            units: 'metric',
            APPID: API_KEY,
        },
    });

    console.log(data.list);

    return data.list;

};
